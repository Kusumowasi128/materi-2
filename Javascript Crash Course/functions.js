// // FUNCTIONS
function greet(greeting = 'Hello', name) {
    if(!name) {
        console.log(greeting);
    return greeting;
    } else {
        console.log(`${greeting} ${name}`);
    return `${greeting} ${name}`;
    }
}


// // ARROW FUNCTIONS
//const greet = (greeting = 'Hello', name = 'There') => `${greeting} ${name}`;
//console.log(greet('Hi'));
